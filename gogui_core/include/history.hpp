#pragma once
#include "point.hpp"
#include "container.hpp"
#include "line.hpp"
#include "supervisor.hpp"

namespace gogui {

    class History {
    public:
        class State {
        private:
            std::vector<Point> points;
            std::vector<Line> lines;

        public:
            State();

            State(const std::vector<Point> &statePoints, const std::vector<Line> &stateLines);

            const std::vector<Point> &getPoints() const;

            const std::vector<Line> &getLines() const;
        };

    private:
        History();

        History(const History &);

        std::vector<State> states;

    public:
        static History &getInstance();

        void putState();

        const std::vector<State> &getStates() const;
    };

    void snapshot();
}