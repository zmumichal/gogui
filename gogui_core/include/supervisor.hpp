#pragma once
#include <unordered_set>
#include "point.hpp"
#include "line.hpp"

namespace gogui {

    class AbstractContainer;

    class Supervisor {

    private:
        Supervisor();

        Supervisor(const Supervisor &);

        std::unordered_set<const AbstractContainer *> existingContainers;

    public:
        static Supervisor &getInstance();

        void registerContainer(const AbstractContainer *containerPtr);

        void unregisterContainer(const AbstractContainer *containerPtr);

        const std::unordered_set<const AbstractContainer *> &getRegisteredContainers() const;
    };
}
