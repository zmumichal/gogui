#include "history.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <vector>

#include "cppjson.h"

namespace gogui {

    class JSONPrinter {
    private:
        const std::vector<History::State> &history;

        typedef unsigned int PointID;
        typedef unsigned int LineID;

        class InternalLine {
        public:
            PointID point1id, point2id;

            bool operator==(const InternalLine &that) const;

            bool operator<(const InternalLine &that) const;

            void normalize();
        };

        std::vector<Point> points;
        std::vector<InternalLine> lines;

    public:
        explicit JSONPrinter(const std::vector<History::State> &_history);

        std::string getJSON();

    protected:

        json::Value getJSONPointsDefinition();

        json::Value getJSONLinesDefinition();

        json::Value getJSONAllStates();

        static GeoObject::Status mergeStatus(GeoObject::Status s1, GeoObject::Status s2);

        static std::string statusToString(GeoObject::Status s);

        json::Value getJSONState(const History::State &state);

    private:

        void getPoints();

        PointID getPointID(const Point &point);

        void getLines();

        LineID getLineID(const InternalLine &iline);

        LineID getLineID(Line line);
    };

    void printJSON();

    std::string getJSON();
}
