#pragma once
#define PI 3.141592653
#define EPSILON 1.0e-12

#include "point.hpp"
#include <cmath>

namespace gogui {

    class Line : public GeoObject {
    private:
        struct Parameters {
            double A;           //Ax+By+C=0
            double B;
            double C;
        };

        Parameters parameters;

        double getA();

        double getB();

        double getC();

        bool compareDouble(const double a, const double b) const;

    public:
        Line(const Point &p1, const Point &p2, std::string color = "");

        const Point point1;
        const Point point2;
    private:
        bool isEqualToWithOrder(const Line &that) const;

        bool isEqualToReversed(const Line &that) const;

    public:
        bool operator==(const Line &that) const;

        bool operator!=(const Line &that) const;

        bool isParallel(const Line &line) const;

        bool isPerpendicular(const Line &line) const;

        double distance(const Line &line) const;

        double distance(const Point &p) const;

        double angleBetweenLines(const Line &line) const;
    };
}
