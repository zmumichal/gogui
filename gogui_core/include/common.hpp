#pragma once
#include <cmath>
#include <string>

namespace gogui {

    class GeoObject {
    public:
        std::string getColor() const;

        void setColor(std::string color);

        enum class Status {
            Normal, Active, Processed
        };

        Status getStatus() const;

        void setStatus(Status s);

        bool compareDouble(const double a, const double b) const;

    private:
        Status status = Status::Normal;
        std::string color = "";
    };
}
