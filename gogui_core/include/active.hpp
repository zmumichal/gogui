#pragma once

#include "point.hpp"
#include "container.hpp"
#include "line.hpp"
#include "supervisor.hpp"

namespace gogui {
    class ActivePoint : public Point, public AbstractContainer {
    public:
        ActivePoint(const Point &point);

        void forEachPoint(PointAcceptor f) const;

        void forEachLine(LineAcceptor) const;
    };

    class ActiveLine : public Line, public AbstractContainer {
    public:
        ActiveLine(const Line &line);

        ActiveLine(const Point &p1, const Point &p2);

        void forEachPoint(PointAcceptor) const;

        void forEachLine(LineAcceptor f) const;
    };
}