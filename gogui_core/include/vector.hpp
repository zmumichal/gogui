#pragma once

#include <cstdio>
#include <vector>

#include "container.hpp"
#include "line.hpp"

namespace gogui {
    template<typename T>
    class vector : public std::vector<T>, public AbstractContainer {
        static_assert(std::is_base_of<GeoObject, T>::value, "gogui containers can hold only gogui geo objects");

    public:
        vector() : std::vector<T>() {
        }

        vector(const std::vector<T> &that) : std::vector<T>(that) {
        }

        vector(std::vector<T> &&that) : std::vector<T>(std::forward(that)) {
        }

        enum class VisualizationMethod {
            CLOUD, PATH, CLOSED_PATH
        };

        void visualizeAs(VisualizationMethod vis);

        void forEachLine(LineAcceptor) const final;

        void forEachPoint(PointAcceptor) const final;

    private:
        VisualizationMethod visualizationMethod = VisualizationMethod::CLOUD;
    };
}