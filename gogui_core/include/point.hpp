#pragma once

#include "common.hpp"

#include <cmath>

namespace gogui {

    class Point : public GeoObject {
    public:
        double x, y;

        Point(const double &x1, const double &y1);

        Point(const double &x1, const double &y1, std::string color);

        constexpr Point();

        bool operator==(const Point &that) const;

        bool operator!=(const Point &that) const;

        bool operator<(const Point &that) const;

        double distance(const Point &p) const;

        template<class T>
        double distance(const T &that) const;
    };
}
