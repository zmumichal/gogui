#pragma once
#include "supervisor.hpp"
#include <vector>
#include <functional>

namespace gogui {

    class Point;

    class Line;

    class AbstractContainer {
    public:
        AbstractContainer();

        ~AbstractContainer();

        typedef std::function<void(const Line &)> LineAcceptor;
        typedef std::function<void(const Point &)> PointAcceptor;

        virtual void forEachLine(LineAcceptor) const = 0;

        virtual void forEachPoint(PointAcceptor) const = 0;
    };
}
