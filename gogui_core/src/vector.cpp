#include <cstdio>
#include <vector>

#include "vector.hpp"

namespace gogui {
    template<typename T>
    void vector<T>::visualizeAs(vector<T>::VisualizationMethod vis) {
        visualizationMethod = vis;
    }

    template<>
    void vector<Point>::forEachLine(LineAcceptor f) const {
        if (visualizationMethod == VisualizationMethod::PATH || visualizationMethod == VisualizationMethod::CLOSED_PATH) {
            const Point *previousPoint = nullptr;
            for (const Point &point : *this) {
                if (previousPoint != nullptr) {
                    const Line line(*previousPoint, point);
                    f(line);
                }
                previousPoint = &point;
            }
            if (visualizationMethod == VisualizationMethod::CLOSED_PATH && size() > 2) {
                const Line closingLine(front(), back());
                f(closingLine);
            }
        }
    }

    template<>
    void vector<Point>::forEachPoint(PointAcceptor f) const {
        for (const Point &point : *this) {
            f(point);
        }
    }

    template<>
    void vector<Line>::forEachLine(LineAcceptor f) const {
        for (const Line &line : *this) {
            f(line);
        }
    }

    template<>
    void vector<Line>::forEachPoint(PointAcceptor f) const {
    }
}
