#include "common.hpp"

namespace gogui {

    std::string GeoObject::getColor() const {
        return color;
    }

    void GeoObject::setColor(std::string color) {
        GeoObject::color = color;
    }

    GeoObject::Status GeoObject::getStatus() const {
        return status;
    }

    void GeoObject::setStatus(Status s) {
        status = s;
    }

    bool GeoObject::compareDouble(const double a, const double b) const {
        return fabs(a - b) < 1e-10;
    }
}

