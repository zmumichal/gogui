#include "point.hpp"

namespace gogui {
    Point::Point(const double &x1, const double &y1) : x(x1), y(y1) {
    }

    Point::Point(const double &x1, const double &y1, std::string color) : x(x1), y(y1) {
        this->setColor(color);
    }

    constexpr Point::Point() : x(0), y(0) {
    }

    bool Point::operator==(const Point &that) const {
        return compareDouble(this->x, that.x) && compareDouble(this->y, that.y);
    }

    bool Point::operator!=(const Point &that) const {
        return !(*this == that);
    }

    bool Point::operator<(const Point &that) const {
        if (x != that.x)
            return x < that.x;
        return y < that.y;
    }

    double Point::distance(const Point &p) const {
        return sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
    }

    template<class T>
    double Point::distance(const T &that) const {
        return that.distance(*this);
    }
}
