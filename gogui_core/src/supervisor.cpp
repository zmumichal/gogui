#include <unordered_set>
#include "supervisor.hpp"

namespace gogui {
    Supervisor::Supervisor() = default;

    Supervisor::Supervisor(const Supervisor &) = delete;

    Supervisor &Supervisor::getInstance() {
        static Supervisor instance;
        return instance;
    }

    void Supervisor::registerContainer(const AbstractContainer *containerPtr) {
        existingContainers.insert(containerPtr);
    }

    void Supervisor::unregisterContainer(const AbstractContainer *containerPtr) {
        existingContainers.erase(containerPtr);
    }

    const std::unordered_set<const AbstractContainer *> &Supervisor::getRegisteredContainers() const {
        return existingContainers;
    }
}
