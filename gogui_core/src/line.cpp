#include "line.hpp"

namespace gogui {

    double Line::getA() {
        return point1.y - point2.y;
    }

    double Line::getB() {
        return point2.x - point1.x;
    }

    double Line::getC() {
        return (point1.x * point2.y) - (point2.x * point1.y);
    }

    bool Line::compareDouble(const double a, const double b) const {
        return fabs(a - b) < EPSILON;
    }

    Line::Line(const Point &p1, const Point &p2, std::string color) : point1(p1), point2(p2) {
        parameters.A = getA();
        parameters.B = getB();
        parameters.C = getC();
        if (color.length() > 0) {
            this->setColor(color);
        }
    }

    bool Line::isEqualToWithOrder(const Line &that) const {
        return (point1 == that.point1) && (point2 == that.point2);
    }

    bool Line::isEqualToReversed(const Line &that) const {
        return (point1 == that.point2) && (point2 == that.point1);
    }

    bool Line::operator==(const Line &that) const {
        return isEqualToWithOrder(that) || isEqualToReversed(that);
    }

    bool Line::operator!=(const Line &that) const {
        return !(*this == that);
    }

    bool Line::isParallel(const Line &line) const {
        return compareDouble(parameters.A, line.parameters.A) && compareDouble(parameters.B, line.parameters.B);
    }

    bool Line::isPerpendicular(const Line &line) const {
        return compareDouble(parameters.A * line.parameters.A, -parameters.B * line.parameters.B);
    }

    double Line::distance(const Line &line) const {
        if (!this->isParallel(line))
            return 0;
        return (fabs(parameters.C - line.parameters.C)) / (sqrt(parameters.A * parameters.A + parameters.B * parameters.B));
    }

    double Line::distance(const Point &p) const {
        return fabs(parameters.A * p.x + parameters.B * p.y + parameters.C) / sqrt(parameters.A * parameters.A + parameters.B * parameters.B);
    }

    double Line::angleBetweenLines(const Line &line) const {
        if (isPerpendicular(line)) return PI / 2;
        return atan((parameters.A * line.parameters.B - line.parameters.A * parameters.B) / (parameters.A * line.parameters.A + parameters.B * line.parameters.B));
    }
}
