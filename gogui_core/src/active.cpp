#include "active.hpp"

namespace gogui {
    ActivePoint::ActivePoint(const Point &point) : Point(point) {
        setStatus(Status::Active);
    }

    void ActivePoint::forEachPoint(PointAcceptor f) const {
        f(*this);
    }

    void ActivePoint::forEachLine(LineAcceptor) const {
    }

    ActiveLine::ActiveLine(const Line &line) : Line(line) {
        setStatus(Status::Active);
    }

    ActiveLine::ActiveLine(const Point &p1, const Point &p2) : ActiveLine(Line(p1, p2)) {
    }

    void ActiveLine::forEachPoint(PointAcceptor) const {
    }

    void ActiveLine::forEachLine(LineAcceptor f) const {
        f(*this);
    }
}