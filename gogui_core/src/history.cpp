#include <vector>
#include "history.hpp"

namespace gogui {

    History::State::State() = default;

    History::State::State(const std::vector<Point> &statePoints, const std::vector<Line> &stateLines) : points(statePoints),
                                                                                                        lines(stateLines) {
    }

    const std::vector<Point> &History::State::getPoints() const {
        return points;
    }

    const std::vector<Line> &History::State::getLines() const {
        return lines;
    }

    History::History() = default;

    History::History(const History &) = delete;

    History &History::getInstance() {
        static History instance;
        return instance;
    }

    void History::putState() {
        std::vector<Point> points;
        std::vector<Line> lines;

        const auto &containers = Supervisor::getInstance().getRegisteredContainers();
        for (const AbstractContainer *abstractContainer : containers) {
            abstractContainer->forEachPoint([&points](const Point &p) {
                points.push_back(p);
            });
            abstractContainer->forEachLine([&lines](const Line &l) {
                lines.push_back(l);
            });
        }

        states.emplace_back(points, lines);
    }

    const std::vector<History::State> &History::getStates() const {
        return states;
    }

    void snapshot() {
        History::getInstance().putState();
    }
}
