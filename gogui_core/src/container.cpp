#include "container.hpp"

namespace gogui {

    class Point;

    class Line;

    AbstractContainer::AbstractContainer() {
        Supervisor::getInstance().registerContainer(this);
    }

    AbstractContainer::~AbstractContainer() {
        Supervisor::getInstance().unregisterContainer(this);
    }
}
